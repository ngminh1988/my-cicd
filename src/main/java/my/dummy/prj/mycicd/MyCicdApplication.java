package my.dummy.prj.mycicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyCicdApplication {

    public static void main(String[] args) {
        HelloWorld myStmt = new HelloWorld(1, "Not today");
        SpringApplication.run(MyCicdApplication.class, args);
    }

}
